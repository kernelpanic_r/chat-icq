const net = require('net');

const client = net.connect(3000);

	client.on('data', function(msg){ 
		console.log(msg.toString()); 
	});

	client.on('end', function(){
		process.exit();
	});

	process.stdin.on('readable', function(){
		let msg = process.stdin.read();
		if(msg === null) return;	 
		msg = msg.toString().replace(/\n/, '');
		if(msg === "/-exit") exit();
		else if (msg.substring(0,5) === "/-ip ") verifyIp(msg);	
		else if(msg.substring(0,6) === "/-time") timeUp(msg.substring(7, msg.length));
		else client.write(msg);
	});
	

	client.on('connect', function(){
	  client.write(sendNick());	  
	});
	
	const ownNickName = () => process.argv.slice(2).length > 0;
	const sendNick = function() {
	   if(ownNickName() === true) return(process.argv.slice(2).toString());	  
		return "no-name";
	};
	
	const exit = () => process.exit();
	
	const verifyIp = function(msg){
	  let address = msg.replace("/-ip ", "").trim();
	  if(address.length === 0 ) console.log("Insert an IP!");
	  else {
	  let value = net.isIP(address);
	  if(value === 4) console.log("The " + address + " is a Ipv4");
	  else if(value === 6) console.log("The " + address + "is a Ipv6");
	  else console.log("Invalid ip address");
	  }
	  };
	  
	  const timeUp = function(param){
	    if(param === "-s" || param === "-seconds")console.log(process.uptime());
	    else if(param === "-m" || param === "-minutes") console.log(process.uptime() / 60);
	    else console.log("Not a valid statement...\n-s, -m");  
	  };
	  