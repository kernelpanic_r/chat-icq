const net = require('net');
const file = require('./file.js');
const crypto = require("./crypto_r");

var connections = [];
var online = [];
var allMessages = [];
var nicks = ['kernelpanic_r', 'rambosaiyan3', 'feeps', 'scharzes', 'pitty', 'salomon', 'baixinho',
'phenomenal', 'arnold', 'tux', 'linux', "elliot", "sharon", "tyrel"];

net.createServer(function(connection){
	let clientName = null;
	let i = 0;
	let cryptFile = false;

	connections.push(connection);

	connection.on('data', function(msg){
		if(i++ === 0) {
			if(msg.toString() !== "no-name")clientName = msg.toString();
			else clientName = makeNick();
			online.push(clientName);
			welcomeMessage(connection, clientName);
			return;
		}
		msg = msg.toString();
		if(msg.substring(0,4) === "/-on") usersOnline(connection);
		else if(msg.substring(0,6) === "/-show") showMyNick(connection, clientName);
		else if(msg.substring(0,6) === "/-nick") clientName = changeNick(connection, msg, clientName);	
		else if(msg.substring(0,6) === "/-help") showHelp(connection);
		else if(msg.substring(0,6) === "/-save") saveFile(msg, connection, cryptFile);
		else if(msg.substring(0,7) === "/-crypt") cryptFile = !cryptFile;
		else sentMessages((clientName + " said: " + msg), connection);
	});

	connection.on('end', function(){
		sentMessages(clientName + " has left... ", connection);
		connections.splice(connections.indexOf(connection), 1);
		online.splice(online.indexOf(clientName), 1);
	});
}).listen(3000);

const usersOnline = function(connection){
	let user = "";
	online.forEach(function(value){
		user = user + ", " + value;
	});
	connection.write(user.slice(2, user.length).toString()); 
};
const welcomeMessage = function(connection, clientName){
	connection.write("Welcome " + clientName);
	connection.write("\nFor help type: /-help");
	sentMessages(clientName + " joined...",connection);   
};
const changeNick = function(connection, msg, clientName){
	let newNick = msg.toString().substring(7, msg.toString().trim().length);
	sentMessages((clientName+" is now: " + newNick), connection);
	return newNick;
};
const showMyNick = function (connection, clientName){
	connection.write("Your nick is: " + clientName);  
};
const showHelp = function(connection) {
	let help = "To change your nick, type:  /-nick";
	help += "\nTo show your nick, type:  /-show";
	help += "\nTo show the users that are online, type: /-on";
	help += "\nTo exit, type: /-exit";
	help += "\nSave the chat, type: /-save fileName";
	help += "\nCrypt your chat, type: /-crypt";
	help += "\nTo show the time that you are up, type: /-time param";
	connection.write(help.toString());
};
const saveMessages = function(){
	let msg = "";
	allMessages.forEach(function(value){
		msg += value + "\n";      
	});
	return msg;
};
const saveFile = function(msg, connection, cryptFile){
	let fileSave = msg.substring(7,msg.length);
	if(!fileSave.trim().length > 0) {
		connection.write("You need to type the name of the file!");
		return;
	}
	let fileContent = saveMessages();
	file((fileSave), fileContent);
	if(cryptFile)crypto.encryptFile(fileSave, fileSave);
	connection.write("Your file was successful created...");
};
const makeNick = () => {
	let value = Math.floor(Math.random() * nicks.length);
	let nick = nicks[value];
	nicks.splice(nicks[value], 1);
	return nick;
};
const sentMessages = function(msg, origin){
	connections.forEach(function(connection){
		if(connection === origin) return;
		connection.write(msg);
		allMessages.push(msg);
	});
};