const crypto = require('crypto');
const file = require('fs');

const cipher = crypto.createCipher('aes192', 'a passsword');
const decipher = crypto.createDecipher('aes192', 'a passsword');

const encryptFile = function(fileName, fileNameEncrypt){
	let openFile = file.createReadStream(fileName);
	let outputFile = file.createWriteStream(fileNameEncrypt);
	openFile.pipe(cipher).pipe(outputFile);
};
const decryptFile = function(fileNameEncrypt, fileName){
	let openFile = file.createReadStream(fileNameEncrypt);
	let outputFile = file.createWriteStream(fileName);
	openFile.pipe(decipher).pipe(outputFile);
};

module.exports = {
	encryptFile : encryptFile,
	decryptFile : decryptFile
};